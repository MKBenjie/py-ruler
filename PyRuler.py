
class Ruler:
    def __init__(self):
        self.__length_num = int(input('Enter length of ruler: \n'))
        self.__length_marks = 4 

    # this will work on the marks "-"
    def marks(self, mark_length, mark_label = " "):
        line = '-' * mark_length
        line += ' ' + mark_label
        print(line)

    # this creates the ruler interval of '1-2-1-3-1-2-1'
    def ruler_interval(self, center_marks):
        if center_marks > 0:
            # creates the first upper interval
            self.ruler_interval(center_marks - 1)
            self.marks(center_marks)
            # creates the second lower interval
            self.ruler_interval(center_marks - 1)
    
    def draw_ruler(self):
        self.marks(self.__length_marks, '0')
        for i in range(1, 1 + self.__length_num):
            self.ruler_interval(self.__length_marks - 1)
            self.marks(self.__length_marks, str(i))

ruler = Ruler()
ruler.draw_ruler()
